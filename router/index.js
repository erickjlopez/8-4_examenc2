const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");


router.get("/compania", (req, res) => {
	const valores = {
		numRecibo: req.query.numRec,
		name: req.query.nombre,
		address: req.query.domicilio,
		tipoDeServicio: req.query.tipoServicio,
		kws: req.query.kilowatts,
		costo: req.query.costo,
		impuesto16: req.query.impuesto,
	}
	res.render("compania.html", valores);
});

router.post("/compania", (req, res) => {
	const valores = {
		numRec: req.body.numRec,
		name: req.body.nombre,
		address: req.body.domicilio,
		tipoDeServicio: req.body.tipoServicio,
		kws: req.body.kilowatts,
		costo: req.body.costo,
		impuesto16: req.body.impuesto,
	}
	res.render("companiaFinal.html", valores);
});

module.exports = router;
